const mongoose = require("mongoose");

const schema = mongoose.Schema({
  first_name: String,
  last_name: String,
  email: String,
  gender: String,
  salary: Number,
});

module.exports = mongoose.model("users", schema);
