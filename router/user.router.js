const express = require('express');
const router = express.Router()
const {
  getUsers,
  createUser,
  getUserById,
  updateUser,
  deleteUser,
} = require('../controller/usercontroller')

router.get('/', getUsers)

router.post('/new', createUser)

router.put('/:id', updateUser)

router.get('/:id', getUserById)

router.delete('/:id', deleteUser)

module.exports = router;
