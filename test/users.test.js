const express = require("express");
const app = express();
const users = require("../models/usermodel");
require("dotenv").config();
const mongoose = require("mongoose");
const supertest = require("supertest");
const controller = require("../router/user.router");

app.use("/users", controller);

beforeEach((done) => {
  mongoose.connect(process.env.MONGODB_TEST,
    { useNewUrlParser: true }, () =>
    done()
  );
});

afterEach((done) => {
  mongoose.connection.db.dropDatabase(() => {
    mongoose.connection.close(() => done());
  });
});

test("GET /users", async () => {
  const post = await users.create({
    first_name: "Test",
    last_name: "t",
    gender: "male",
    email: "test@gmail.com",
    salary: 230000,
  });

  await supertest(app)
    .get("/users/")
    .expect(200)
    .then((response) => {
      expect(Array.isArray(response.body)).toBeTruthy();
      expect(response.body.length).toEqual(1);
      // console.log(response.body.length);
      expect(response.body[0].first_name).toBe(post.first_name);
      expect(response.body[0].last_Name).toBe(post.last_Name);
      expect(response.body[0].gender).toBe(post.gender);
      expect(response.body[0].email).toBe(post.email);
      expect(response.body[0].salary).toBe(post.salary);
    });
});

test("POST /users/new", async () => {
  const data = {
    email: "lerisack8@patch.com",
    first_name: "Laxmi",
    last_name: "Mcsaitterick",
    gender: "Female",
    salary: 776,
  }
  const res = await supertest(app).post("/users/new")
  .send(data)
  .expect(200)
  console.log(res.request._data);
  // expect(res.res.statusMessage).toBe("Created Successfuly");
  // expect(res.statusCode).toBe(200);
  // expect(res.statusCode).toEqual(400);
  console.log(res.statusCode)
  // expect(res.body).toHaveProperty("post");
  // await supertest(app).post("/users/new")
  //   .send(data)
  //   .then(async (response) => {
  //     console.log(response.statusCode)
  //     // Check the response
  //     // expect(response.body.message).toBe("Created Successfully.");

  //     // Check data in the database
  //     // const post = await Post.findOne({ email: response.body.email });
  //     // expect(post).toBeTruthy();
  //     // expect(post.first_name).toBe(data.first_name);
  //     // expect(post.last_Name).toBe(data.last_Name);
  //   });
});
