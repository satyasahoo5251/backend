const express = require("express");
const app = express();
const db = require("./db/db");
const models = require("./models/usermodel");
const userRouter = require("./router/user.router");
const bodyparser = require("body-parser");
const cors = require("cors")

const port = 3000;

app.get('/',(req,res) => {
    res.send('Welcome');
})

app.use(cors({origin: true}));
app.use(bodyparser.json());
app.use(express.urlencoded({extended: true}));
app.use("/users",userRouter);
app.listen(port,() => {
    console.log(`Server running at port no ${port}`);
});

module.exports = app;