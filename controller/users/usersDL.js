const User = require("../../models/usermodel");
const json = require("jsonwebtoken");

const getUsersDL = async (req) => {
  try {
    const result = await User.find();
    return result;
  } catch (error) {
    throw error;
  }
};

const createUserDL = async (req, res) => {
  try {
    let Users = await User.findOne({
      email: req.body.email,
    });
    if (Users && Users.email.length > 0) {
      throw "User Existed";
    }
    let newuser = new User({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      gender: req.body.gender,
      email: req.body.email,
      salary: req.body.salary,
    });
    await newuser.save();
    let payload = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      gender: req.body.gender,
      email: req.body.email,
      salary: req.body.salary,
    }
    // let result = json.sign(payload, "hgghh");
    return payload;
    // ({
    //   message: "Created Successfully.",
    //   token: result
    // })

    // return result;
  } catch (error) {
    throw error;
  }
};
const getUserByIdDL = async (req) => {
  try {
    let user = await User.findOne(
      {
        _id: req.params.id,
      }
    );
    return user;
  } catch (error) {
    throw error;
  }
};
const updateUserDL = async (req) => {
  try {
    const filter = { _id: req.params.id };
    let updateUser = {
      $set: {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        gender: req.body.gender,
        email: req.body.email,
        salary: req.body.salary,
      },
    };

    const result = await User.updateOne(filter, updateUser);
    return result;
  } catch (error) {
    throw error;
  }
};

const deleteUserDL = async (req) => {
  try {
    let Users = await User.findOne({
      _id: req.params.id,
    });
    if (Users !== null) {
      let result = await User.deleteOne({ _id: req.params.id });
      return {
        message: "Deleted Successfuly.",
        status: 200,
        deletedCount: result.deletedCount,
      };
    } else {
      throw new Error("Not found with Id");
    }
  } catch (error) {
    throw error;
  }
};
module.exports = {
  getUsersDL,
  createUserDL,
  getUserByIdDL,
  updateUserDL,
  deleteUserDL,
};
