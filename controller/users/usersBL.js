const json = require('jsonwebtoken')
const {
    getUsersDL,
    createUserDL,
    getUserByIdDL,
    updateUserDL,
    deleteUserDL,
  } = require('./usersDL');

const getUsersBL = async (req) => {
  try {
    const result = await getUsersDL(req);
    return result;
  } catch (error) {
    throw error;
  }
};

const createUserBL = async (req, res) => {
  try {
    const result = await createUserDL(req, res);
    return result;
  } catch (error) {
    throw error;
  }
};

const getUserByIdBL = async (req) => {
  try {
    const result = await getUserByIdDL(req);
    return result;
  } catch (error) {
    throw error;
  }
};

const updateUserBL = async (req) => {
  try {
    const result = await updateUserDL(req);
    return result;
  } catch (error) {
    throw error;
  }
};

const deleteUserBL = async (req) => {
  try {
    const result = await deleteUserDL(req);
    return result;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getUsersBL,
  createUserBL,
  getUserByIdBL,
  updateUserBL,
  deleteUserBL,
};