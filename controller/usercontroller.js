const {
  getUsersBL,
  createUserBL,
  getUserByIdBL,
  updateUserBL,
  deleteUserBL,
} = require("./users/usersBL");

const getUsers = async (req, res) => {
  try {
    const result = await getUsersBL(req);
    return res.send(result);
  } catch (error) {
    if(error.message.includes("Cast to ObjectId failed") === true) {
      return res.status(400).send({ message: "Not Found Any User" });
    }
    return res.status(400).send({ message: error });
  }
};

const createUser = async (req, res) => {
  try {
    const result = await createUserBL(req);
    return res.status(200).send(result);
  } catch (error) {
    if(error.message && error.message.includes("Cast to ObjectId failed") === true) {
      return res.status(400).send({ message: "Not Found Any User" });
    }
    return res.status(400).send({ message: error });
  }
};

const getUserById = async (req, res) => {
  try {
    const result = await getUserByIdBL(req, res);
    return res.send(result);
  } catch (error) {
    if (error.message.includes("Cast to ObjectId failed") === true) {
      return res.status(400).send({
        message: "User Not Found!",
      });
    }
    return res.status(400).send(error);
  }
};

const updateUser = async (req, res) => {
  try {
    const result = await updateUserBL(req);

    return res.send(result);
  } catch (error) {
    if (error.message.includes("Cast to ObjectId failed") === true) {
      return res.status(400).send({ message: "Not Found Any User" });
    }
    return res.status(400).send({ message: error });
  }
};

const deleteUser = async (req, res) => {
  try {
    const result = await deleteUserBL(req);
    return res.send(result);
  } catch (error) {
    if (error.message.includes("Not found with Id") === true) {
      return res.status(400).send({ message: "Not Found Any User" });
    }
    return res.status(400).send({ message: error });
  }
};

module.exports = {
  getUsers,
  createUser,
  getUserById,
  updateUser,
  deleteUser,
};
