const mongoose = require('mongoose')
require('dotenv').config()
mongoose.connect(process.env.MONGODB_URL)
.then((e) => {
    console.log("Connected Succesfull");
})
.catch((e) => {
    console.log("Connected Failed");
})
module.exports = mongoose;